from django.test import TestCase

import datetime
from django.utils import timezone

from .models import Question, Choice


class ChoiceModelTest(TestCase):
    def test_choice_with_bad_questionID(self):
        question = Question(question_id=-3, pub_date=timezone.now())
        choice = Choice(question=question, votes=-3) 
        self.assertIs(choice.question.is_a_valid_question(), False)
    
    def test_choice_with_bad_vote_number(self):
        question = Question(pub_date=timezone.now())
        choice = Choice(question=question, votes=-3) 
        self.assertIs(choice.is_a_valid_vote_number(), False)